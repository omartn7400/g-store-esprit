import 'package:flutter/material.dart';

class Signup extends StatefulWidget {
  const Signup({Key? key}) : super(key: key);

  @override
  State<Signup> createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  late String? _username;
  late String? _password;
  late String? _addresse;
  late String? _annee;
  late String? _email;

  final GlobalKey<FormState> _keyForm = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Inscription"),
        ),
        body: Form(
          key: _keyForm,
          child: ListView(
            children: [
              Container(
                  width: double.infinity,
                  margin: const EdgeInsets.fromLTRB(20, 0, 20, 10),
                  child: Image.asset("assets/images/minecraft.jpg", width: 460, height: 215)
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                child:  TextFormField(
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: "Username"),
                  onSaved: (String? value){
                    _username = value;
                    //  print(_username);
                  },
                  validator: (String? value){
                    if (value!.isEmpty || value == null){
                      return "username ne doit pas etre vide";
                    }else if(value.length < 5 ){
                      return "username doit etre minimum 5 charactere";
                    }
                    else{
                      return null;
                    }
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                child:  TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: "Email"),
                  onSaved: (String? value){
                    _email = value;
                    //  print(_username);
                  },
                  validator: (String? value){
                    if (value!.isEmpty || value == null){
                      return "email ne doit pas etre vide";
                    }
                    else{
                      return null;
                    }
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                child:  TextFormField(
                  obscureText: true,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: "Mot de passe"),
                  onSaved: (String? value){
                    _password = value;
                    //  print(_username);
                  },
                  validator: (String? value){
                    if (value!.isEmpty || value == null){
                      return "password ne doit pas etre vide";
                    }else if(value.length < 5){
                      return "password doit etre valide";
                    }
                    else{
                      return null;
                    }
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                child:  TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: "Année de naissance"),
                  onSaved: (String? value){
                    _annee = value;
                    //  print(_username);
                  },
                  validator: (String? value){
                    if (value!.isEmpty || value == null){
                      return "annee de naissance ne doit pas etre vide";
                    }else if(int.parse(value.toString())>2021){
                      return "annee de naissance doit etre valide";
                    }
                    else{
                      return null;
                    }
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                child:  TextFormField(
                  maxLines: 6,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: "Adresse de facturation"),
                  onSaved: (String? value){
                    _addresse = value;
                    //  print(_username);
                  },
                  validator: (String? value){
                    if (value!.isEmpty || value == null){
                      return "addresse ne doit pas etre vide";
                    }else if(value.length < 5 ){
                      return "addresse doit etre valide";
                    }
                    else{
                      return null;
                    }
                  },
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    child: const Text("S'inscrire"),
                    onPressed: () {
                      if(_keyForm.currentState!.validate()){
                        _keyForm.currentState!.save();
                        showDialog(
                          context: context,
                          builder: (BuildContext context){
                            return AlertDialog(
                              title: const Text("information"),
                              content: Text("username : " + _username! + "\n" +
                                  "Email : " + _email! + "\n" +
                                  "password : " + _password! + "\n" +
                                  "annee de naissance : " + _annee! + "\n" +
                                  "addresse : " + _addresse! + "\n"),
                            );
                          },
                        );
                      }
                    },
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  ElevatedButton(
                    child: const Text("Annuler"),
                    onPressed: () {
                      _keyForm.currentState!.reset();
                    },
                  )
                ],
              )
            ],
          ),)
    );
  }
}
