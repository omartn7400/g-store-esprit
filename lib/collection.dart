import 'package:flutter/material.dart';
import 'package:workshop_gamix2122/product_info.dart';

import 'collection_item.dart';


class Collection extends StatefulWidget{
  const Collection({Key? key}) : super(key: key);

  @override
  State<Collection> createState() => _CollectionState();
}

class _CollectionState extends State<Collection> {

  final List<Product> _produits = [];

  @override
  void initState() {
    _produits.add(Product("assets/images/dmc5.jpg", "Devil May Cry 5",100));
    _produits.add(Product("assets/images/re8.jpg", "Resident Evil VIII",150));
    _produits.add(Product("assets/images/nfs.jpg", "Need For Speed Heat",200));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Ma Collection"),
      ),
      body: GridView.builder(
        itemCount: _produits.length,
        itemBuilder: (BuildContext context, int index){
          return collectionItem(_produits[index].image, _produits[index].title);
        },
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5,
          mainAxisExtent: 150,
      ),
      )
    );
  }
}
