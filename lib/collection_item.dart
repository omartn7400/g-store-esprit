import 'package:flutter/material.dart';

class collectionItem extends StatelessWidget{
  final String _image;
  final String _title;

  const collectionItem(this._image, this._title, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Container(
          width: 155,
          margin: const EdgeInsets.all(20),
          child: Column(
            children: [
              Image.asset(_image),
              const SizedBox(height: 10),
              Text(_title),
            ],
          ),
        ),
      );

  }


}