import 'package:flutter/material.dart';

import 'product_info.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  final List<Product> _produits = [];

  @override
  void initState(){
    ProductInfo("assets/images/dmc5.jpg", "Devil May Cry 5", 200);
    ProductInfo("assets/images/re8.jpg", "Resident Evil VIII", 200);
    ProductInfo("assets/images/nfs.jpg", "Need For Speed Heat", 100);
    ProductInfo("assets/images/rdr2.jpg", "Red Dead Redemption II", 150);
    ProductInfo("assets/images/fifa.jpg", "FIFA 22", 100);
    ProductInfo("assets/images/Minecraft.jpg", "Minecraft", 100);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("G-Store ESPRIT"),
      ),
      body: ListView.builder(
          itemCount: _produits.length,
          itemBuilder: (BuildContext context, int index){
            return ProductInfo(_produits[index].image, _produits[index].title, _produits[index].price);
          },
      ),
    );
  }
}